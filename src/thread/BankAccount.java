package thread;

public class BankAccount {
	
	private double balance;
	
	public BankAccount(double amount){
		this.balance = amount;
		
	}
	
	public void deposit(double amount){
		balance += amount;
	}
	
	public void withDraw(double amount){
		balance -= amount;
	}
	
	public double getBalance(){
		return balance;
	}

}
