package multithread;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
   A bank account has a balance that can be changed by 
   deposits and withdrawals.
 */
public class BankAccount
{
	private double balance;
	private Lock balanceChangedLock;
	private Condition sufficientFundsCondition;
	private double interestRate;
	/**
      Constructs a bank account with a zero balance.
	 */
	public BankAccount()
	{
		balance = 0;
		interestRate = 0;
		balanceChangedLock = new ReentrantLock();
		sufficientFundsCondition = balanceChangedLock.newCondition();
	}
	
	public void setInterestRate(double rate){
		this.interestRate = rate;
	}
	public void applyInterest(){
		balanceChangedLock.lock();
		
		try{
			this.balance += this.balance * this.interestRate;
		}
		finally{
			balanceChangedLock.unlock();
		}
	}

	/**
      Deposits money into the bank account.
      @param amount the amount to deposit
	 */
	public void deposit(double amount)
	{
		balanceChangedLock.lock();
		try{
			System.out.print("Depositing " + amount);
			double newBalance = balance + amount;
			System.out.println(", new balance is " + newBalance);
			balance = newBalance;
		}
		finally{
			balanceChangedLock.unlock();
		}
		}

	/**
      Withdraws money from the bank account.
      @param amount the amount to withdraw
	 * @throws InterruptedException 
	 */
	public void withdraw(double amount) throws InterruptedException
	{	
		balanceChangedLock.lock();
		try{
			while (balance < amount)
				 sufficientFundsCondition.await();
			System.out.print("Withdrawing " + amount);
			double newBalance = balance - amount;
			System.out.println(", new balance is " + newBalance);
			balance = newBalance;
		}
		finally{
			balanceChangedLock.unlock();
		}
	}

	/**
      Gets the current balance of the bank account.
      @return the current balance
	 */
	public double getBalance()
	{
		return balance;
	}
}
